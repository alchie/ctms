<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('concerts/concerts_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">

	    <div class="panel panel-default">

	    	<div class="panel-heading">
	    		<h3 class="panel-title">Add Guest</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

				<div class="form-group">
	    			<label>Name</label>
	    			<input name="name" type="text" class="form-control" value="<?php echo $this->input->post('name'); ?>">
	    		</div>

	    		<div class="form-group">
	    			<label>Phone Number</label>
	    			<input name="phone_number" type="text" class="form-control" value="<?php echo $this->input->post('phone_number'); ?>">
	    		</div>

	    		<div class="form-group">
	    			<label>Email Address</label>
	    			<input name="email" type="text" class="form-control" value="<?php echo $this->input->post('email'); ?>">
	    		</div>

	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("guests/index/{$concert->id}"); ?>" class="btn btn-warning">Back</a>
	    	</div>
	    	</form>
	    </div>

    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>