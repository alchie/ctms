<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('concerts/concerts_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<a href="<?php echo site_url("guests/add/{$concert->id}"); ?>" class="btn btn-success btn-xs pull-right">Add Guest</a>
	    		<h3 class="panel-title">Guests</h3>
	    	</div>
	    	<div class="panel-body">
<?php if( $guests ) { ?>
	    		<table class="table table-default">
	    			<thead>
	    				<tr>
	    					<th>Name</th>
	    					<th>Phone Number</th>
	    					<th>Email Address</th>
	    					<th width="10px">Action</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($guests as $guest) { 

	    				?>
	    				<tr>
	    					<td><?php echo $guest->name; ?></td>
	    					<td><?php echo $guest->phone_number; ?></td>
	    					<td><?php echo $guest->email; ?></td>
	    					<td><a href="<?php echo site_url("guests/edit/" . $guest->id); ?>" class="btn btn-warning btn-xs">Edit</a></td>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

	    		<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>
<?php } else { ?>
	<center>
		<strong>
			No Guests Found!
		</strong>
	</center>
<?php } ?>

	    	</div>
	    </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>