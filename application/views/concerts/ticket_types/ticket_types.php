<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('concerts/concerts_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<a href="<?php echo site_url("ticket_types/add/{$concert->id}"); ?>" class="btn btn-success btn-xs pull-right">Add Type</a>
	    		<h3 class="panel-title">Ticket Types</h3>
	    	</div>
	    	<div class="panel-body">

<?php if( $types ) { ?>

	    		<table class="table table-default">
	    			<thead>
	    				<tr>
	    					<th>Name</th>
	    					<th class="text-center">Price</th>
	    					<th class="text-center">Number of Tickets</th>
	    					<th width="10px">Action</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($types as $type) { ?>
	    				<tr>
	    					<td><a href="<?php echo site_url("tickets/type/{$concert->id}/{$type->id}"); ?>"><?php echo $type->name; ?></a></td>
	    					<td class="text-center"><?php echo $type->price; ?></td>
	    					<td class="text-center"><?php echo $type->tickets_count; ?></td>
	    					<td><a href="<?php echo site_url("ticket_types/edit/" . $type->id); ?>" class="btn btn-warning btn-xs">Edit</a></td>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

<?php } else { ?>
	<center>
		<strong>
			No Ticket Types Found!
		</strong>
	</center>

<?php }  ?>

	    	</div>
	    </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>