<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('concerts/concerts_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<a href="<?php echo site_url("distributors/add/{$concert->id}"); ?>" class="btn btn-success btn-xs pull-right">Add Distributor</a>
	    		
	    		<h3 class="panel-title">Distributors</h3>
	    	</div>
	    	<div class="panel-body">
<?php if( $distributors ) { ?>
	    		<table class="table table-default">
	    			<thead>
	    				<tr>
	    					<th>Name</th>
	    					<th>Phone Number</th>
	    					<th class="text-center">Tickets Assigned</th>
	    					<th width="105px">Action</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($distributors as $distributor) { 

	    				?>
	    				<tr>
	    					<td><?php echo $distributor->name; ?></td>
	    					<td><?php echo $distributor->phone_number; ?></td>
	    					<td class="text-center"><a href="<?php echo site_url("distributors/tickets/" . $distributor->id); ?>"><?php echo $distributor->tickets_assigned; ?></a></td>
	    					<td>

	    					<a href="<?php echo site_url("distributors/edit/" . $distributor->id); ?>" class="btn btn-warning btn-xs">Edit</a>
	    					<a href="<?php echo site_url("distributors/tickets/" . $distributor->id); ?>" class="btn btn-success btn-xs">Tickets</a>

	    					</td>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

	    		<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>
<?php } else { ?>
	<center>
		<strong>
			No Distributors Found!
		</strong>
	</center>
<?php } ?>

	    	</div>
	    </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>