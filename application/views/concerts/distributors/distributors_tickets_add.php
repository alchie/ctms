<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('concerts/concerts_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">

	    <div class="panel panel-default">

	    	<div class="panel-heading">
	    		<h3 class="panel-title"><strong><?php echo $distributor->name; ?></strong> - Add Tickets</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">
<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

	    		<div class="form-group">
	    			<label>Ticket Number</label>
	    			<input name="ticket_number" type="text" class="form-control text-center autofocus" value="<?php echo $this->input->post('name'); ?>" autofocus>
	    		</div>

	    		<div class="form-group">
	    			<label>Date Released</label>
	    			<input name="date_released" type="text" class="form-control text-center datepicker" value="<?php echo date('m/d/Y'); ?>">
	    		</div>

	    	</div>

	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("distributors/tickets/{$distributor->id}"); ?>" class="btn btn-warning">Back</a>
	    	</div>
	    	
	    	</form>
	    </div>

    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>