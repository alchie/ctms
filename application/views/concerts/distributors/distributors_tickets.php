<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('concerts/concerts_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    	<a href="<?php echo site_url("distributors/index/{$concert->id}"); ?>" class="btn btn-warning btn-xs pull-right">Back</a>

	    		<a style="margin-right:5px;" href="<?php echo site_url("distributors/tickets_add/{$distributor->id}"); ?>" class="btn btn-success btn-xs pull-right">Assign Tickets</a>

	    		<h3 class="panel-title"><?php echo $distributor->name; ?> Tickets (<?php echo $tickets_count; ?>)</h3>
	    	</div>
	    	<div class="panel-body">

<?php if( $tickets ) { ?>

	    		<table class="table table-default">
	    			<thead>
	    				<tr>
	    					<th>Ticket Number</th>
	    					<th>Ticket Type</th>
	    					<th>Date Released</th>
	    					<th width="10px">Action</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($tickets as $ticket) { 

	    				?>
	    				<tr class="<?php echo ($ticket->paid==1)?'success':''; ?>">
	    					<td><?php echo $ticket->ticket_number; ?></td>
	    					<td><a href="<?php echo site_url("tickets/type/{$concert->id}/$ticket->type_id"); ?>"><?php echo $ticket->name; ?> (P<?php echo $ticket->price; ?>)</a></td>
	    					<td><?php echo date('F d, Y', strtotime($ticket->date_released)); ?></td>
	    					<td>

	    					<a href="<?php echo site_url("distributors/ticket_delete/{$ticket->concert_id}/{$ticket->distributor_id}/{$ticket->ticket_number}"); ?>" class="btn btn-danger btn-xs">Delete</a>

	    					</td>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

	    		<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>
<?php } else { ?>
	<center>
		<strong>
			No Tickets Assigned!
		</strong>
	</center>
<?php } ?>

	    	</div>
	    </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>