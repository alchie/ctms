<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('concerts/concerts_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    	<h3 class="panel-title pull-left">Tickets</h3>
	    	<a href="<?php echo site_url("tickets/add/{$concert->id}"); ?>" class="btn btn-success btn-xs pull-right">Add Tickets</a>

<center class="">
<div class="btn-group" role="group" >
  <a href="<?php echo site_url("tickets/index/{$concert->id}"); ?>" class="btn btn-<?php echo ($output=='index') ? 'warning' : 'default'; ?> btn-xs">All (<?php echo $concert->tickets_count; ?>)</a>
  <a href="<?php echo site_url("tickets/sold/{$concert->id}"); ?>" class="btn btn-<?php echo ($output=='sold') ? 'warning' : 'default'; ?> btn-xs">Sold (<?php echo $concert->sold; ?>)</a>
  <a href="<?php echo site_url("tickets/unsold/{$concert->id}"); ?>" class="btn btn-<?php echo ($output=='unsold') ? 'warning' : 'default'; ?> btn-xs">Unsold (<?php echo $concert->unsold; ?>)</a>
  <a href="<?php echo site_url("tickets/assigned/{$concert->id}"); ?>" class="btn btn-<?php echo ($output=='assigned') ? 'warning' : 'default'; ?> btn-xs">Assigned (<?php echo $concert->assigned; ?>)</a>
  <a href="<?php echo site_url("tickets/unassigned/{$concert->id}"); ?>" class="btn btn-<?php echo ($output=='unassigned') ? 'warning' : 'default'; ?> btn-xs">Unassigned (<?php echo $concert->unassigned; ?>)</a>
</div>
</center>

	    		
	    	</div>
	    	<div class="panel-body">
<?php if( $tickets ) { ?>
	    		<table class="table table-default">
	    			<thead>
	    				<tr>
	    					<th>Ticket Number</th>
	    					<th>Ticket Type</th>
	    					<th>Assigned to</th>
	    					<th width="90px">Action</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($tickets as $ticket) { 

	    				?>
	    				<tr class="<?php echo ($ticket->paid==1)?'success':''; ?>">
	    					<td><?php echo $ticket->ticket_number; ?></td>
	    					<td><a href="<?php echo site_url("tickets/type/{$concert->id}/{$ticket->type_id}"); ?>"><?php echo $ticket->name; ?> (P<?php echo $ticket->price; ?>)</a></td>
	    					<td>
<?php if( $ticket->assigned_to ) { ?>
	    					<a href="<?php echo site_url("distributors/tickets/{$ticket->distributor_id}"); ?>"><?php echo $ticket->assigned_to; ?></a>
	    					<a href="<?php echo site_url("tickets/remove_assignment/" . $ticket->id); ?>" style="color:red;"><span class="glyphicon glyphicon-remove"></span></a>
<?php } ?>
	    					</td>
	    					<td class="text-center">
	    					<a href="<?php echo site_url("tickets/edit/" . $ticket->id); ?>" class="btn btn-warning btn-xs">Edit</a>
	    					<?php if( $ticket->paid == 0 ) { ?>
	    					<a href="<?php echo site_url("tickets/mark_paid/" . $ticket->id); ?>" class="btn btn-success btn-xs">Sold</a>
	    					<?php } ?>
	    					</td>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

	    		<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>
<?php } else { ?>
	<center>
		<strong>
			No Tickets Found!
		</strong>
	</center>
<?php } ?>

	    	</div>
	    </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>