<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('concerts/concerts_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">

	    	 <a href="<?php echo site_url("tickets/delete/" . $ticket->id); ?>" class="btn btn-danger btn-xs pull-right confirm">Delete</a>

	    		<h3 class="panel-title">Edit Ticket</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

	    		<div class="form-group">
	    			<label>Ticket Type</label>
	    			<select class="form-control" name="type_id">
	    				<?php foreach($types as $type) { ?>
	    					<option <?php echo ($ticket->type_id==$type->id) ? 'selected' : ''; ?> value="<?php echo $type->id; ?>"><?php echo $type->name; ?> (P<?php echo $type->price; ?>)</option>
	    				<?php } ?>
	    			</select>
	    		</div>

	    		<div class="form-group">
	    			<label>Ticket Number</label>
	    			<input name="ticket_number" type="text" class="form-control" value="<?php echo $ticket->ticket_number; ?>">
	    		</div>

	    		<div class="form-group">
	    			<label><input name="paid" type="checkbox" value="1" <?php echo ($ticket->paid==1) ? 'checked': ''; ?>> Paid</label>	
	    		</div>

	    		<div class="form-group">
	    			<label>Assign to</label>
	    			<select class="form-control" name="assign_to" title="Select a Distributor">
	    				<option value="no_assignment">- - No Assignment - -</option>
	    				<?php foreach($distributors as $distributor) { ?>
	    					<option <?php echo ($distributor->id==$ticket->distributor_id) ? 'selected' : ''; ?> value="<?php echo $distributor->id; ?>"><?php echo $distributor->name; ?></option>
	    				<?php } ?>
	    			</select>
	    		</div>

	    		<div class="form-group">
	    			<label>Date Released</label>
	    			<input name="date_released" type="text" class="form-control datepicker" value="<?php echo ($ticket->date_released) ? date('m/d/Y', strtotime($ticket->date_released)) : ''; ?>">
	    		</div>

	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("tickets/index/{$concert->id}"); ?>" class="btn btn-warning">Back</a>
	    	</div>
	    	</form>
	    </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>