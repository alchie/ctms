<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('concerts/concerts_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">

<?php if( $types ) { ?>

	    <div class="panel panel-default">

	    	<div class="panel-heading">
	    		<h3 class="panel-title">Add Tickets</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">
<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

	    		<div class="form-group">
	    			<label>Ticket Type</label>
	    			<select class="form-control" name="type_id">
	    				<?php foreach($types as $type) { ?>
	    					<option value="<?php echo $type->id; ?>"><?php echo $type->name; ?> (P<?php echo $type->price; ?>)</option>
	    				<?php } ?>
	    			</select>
	    		</div>

	    		<div class="form-group">
	    			<label>Ticket Number</label>
	    			<input name="ticket_number" type="text" class="form-control" value="<?php echo $this->input->post('ticket_number'); ?>">
	    		</div>

	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("tickets/index/{$concert->id}"); ?>" class="btn btn-warning">Back</a>
	    	</div>
	    	</form>
	    </div>

<?php } else { ?>
	
	<center>
		<strong>
			No Ticket Types Found!
		</strong>
	</center>
	<br>
<?php } ?>

    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>