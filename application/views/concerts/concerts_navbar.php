<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
  <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand"><?php echo $concert->name; ?></div>
    </div>

      <form class="navbar-form navbar-left" role="search">

        <div class="input-group">

          <input name="q" type="text" class="form-control" placeholder="Search" value="<?php echo $this->input->get('q'); ?>">
          <span class="input-group-btn">
          <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
          </span>
          
        </div>

      </form>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $current_page; ?> <span class="caret"></span></a>
          <ul class="dropdown-menu">
<?php 
$url['tickets'] = array('uri' => 'tickets/index/' . $concert->id, 'title'=>'Tickets');
$url['ticket_types'] = array('uri' => 'ticket_types/index/' . $concert->id, 'title'=>'Ticket Types');
$url['distributors'] = array('uri' => 'distributors/index/' . $concert->id, 'title'=>'Distributors');
$url['guests'] = array('uri' => 'guests/index/' . $concert->id, 'title'=>'Guests');
$url['reports'] = array('uri' => 'reports/tickets/' . $concert->id, 'title'=>'Report');
foreach($url as $k=>$v) {
?>
  <li class="<?php echo ($k==$current_uri) ? 'active' : ''; ?>"><a href="<?php echo site_url($v['uri']); ?>"><?php echo $v['title']; ?></a></li>
<?php } ?>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>