<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
<head>
  <title>Print</title>
  <style>
    <!--
    body {
      font-family: Arial;
      padding:0;
      margin:0;
      font-size: 12px;
    }
    .container {
      width: 100%;
    }
    .wrapper {
      border: 1px solid #000;
      padding:10px;
      margin-bottom: 10px;
    }
    .allcaps {
      text-transform: uppercase;
    }
    .label {
      color:#000;
      text-transform: uppercase;
      text-decoration: underline;
    }
    .detail {
      font-size: 12px;
      margin-left: 10px;
      margin-top: 10px;
      font-weight: bold;
    }
    table td {
      vertical-align: middle;
      padding: 3px;
    }
    .border-bottom td {
      border-bottom: 1px solid #000;
    }
    .text-center {
      text-align: center;
    }
     .text-left {
      text-align: left;
    }
    th {
      font-size: 12px;
    }
    td {
      font-size: 12px;
    }
    .signature {
      border-top:1px solid #000;
      text-align: center;
      margin: 40px auto 0;
      font-size: 12px;
      font-weight: bold;
    }
    .header-label {
      margin: 0 0 20px 0;
      border-bottom: 1px solid #000;
      padding: 10px;
      background-color: #EEE;
    }
    .header-label small {
    	font-weight: normal;
    }
    .total {
      font-size: 12px;
      font-weight: bold;
    }
    .text-right {
    	text-align: right;
    }
    table td {
    	border-bottom: 1px solid #CCC;
    }
    tr.overalltotal {
    	background-color: #EEE;
    }
    tr.overalltotal td {
    	padding: 5px;
    	font-size: 14px;
    }
    a {
      text-decoration: none;
      color:#000;
    }
    -->
  </style>
</head>
<body>
<div class="wrapper">

<h3 class="text-center allcaps header-label"><?php echo $concert->name; ?>
<br><small><?php echo date('F d, Y', strtotime($concert->date)); ?> &middot; <?php echo $concert->venue; ?></small>
</h3>

<?php if( $tickets ) { ?>
<table width="100%" cellpadding="0" cellspacing="0">
<thead>
  <tr>
    <th class="text-left">Ticket Number</th>
    <th class="text-left">Assigned to</th>
    <th class="text-left">Ticket Type</th>
    <th class="text-left">Price</th>
    <th class="text-right">Paid</th>
  </tr>
</thead>
	    			<tbody>
<?php foreach($tickets as $ticket) { ?>
  <tr>
    <td><?php echo $ticket->ticket_number; ?></td>
    <td><a class="item" href="<?php echo site_url(uri_string()) . "?distributor=" . $ticket->distributor_id; ?>"><?php echo $ticket->assigned_to; ?></a></td>
     <td><?php echo $ticket->name; ?></td>
     <td><?php echo $ticket->price; ?></td>
     <td class="text-right"><?php echo $ticket->paid; ?></td>
  </tr>
<?php } ?>
	    			</tbody>
	    		</table>
<?php } else { ?>
	<div class="text-center">No Account Found!</div>
<?php } ?>

</div> 
</body>
</html>