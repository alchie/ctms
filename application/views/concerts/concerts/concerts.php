<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<a href="<?php echo site_url("concerts/add"); ?>" class="btn btn-success btn-xs pull-right">Add Concert</a>
	    		<h3 class="panel-title">Concerts</h3>
	    	</div>
	    	<div class="panel-body">
	    		<table class="table table-default">
	    			<thead>
	    				<tr>
	    					<th>Name</th>
	    					<th>Date</th>
	    					<th>Venue</th>
	    					<th class="text-center">Tickets</th>
	    					<th class="text-center">Assigned</th>
	    					<th width="230px">Action</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($concerts as $concert) { ?>
	    				<tr>
	    					<td><?php echo $concert->name; ?></td>
	    					<td><?php echo date('m/d/Y', strtotime($concert->date)); ?></td>
	    					<td><?php echo $concert->venue; ?></td>
	    					<td class="text-center"><?php echo $concert->tickets_count; ?></td>
	    					<td class="text-center"><?php echo $concert->tickets_assigned_count; ?></td>
	    					<td>
	    					<a href="<?php echo site_url("concerts/edit/" . $concert->id); ?>" class="btn btn-warning btn-xs">Edit</a>
	    					<a href="<?php echo site_url("tickets/index/" . $concert->id); ?>" class="btn btn-primary btn-xs">Tickets</a>
	    					<a href="<?php echo site_url("distributors/index/" . $concert->id); ?>" class="btn btn-danger btn-xs">Distributor</a>
	    					<a href="<?php echo site_url("guests/index/" . $concert->id); ?>" class="btn btn-info btn-xs">Guests</a>
	    					</td>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>
	    	</div>
	    </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>