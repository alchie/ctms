<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Add Concert</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

	    		<div class="form-group">
	    			<label>Concert Name</label>
	    			<input name="name" type="text" class="form-control" value="<?php echo $this->input->post('name'); ?>">
	    		</div>
	    		<div class="form-group">
	    			<label>Concert Date</label>
	    			<input name="date" type="text" class="form-control datepicker" value="<?php echo $this->input->post('date'); ?>">
	    		</div>
	    		<div class="form-group">
	    			<label>Venue</label>
	    			<input name="venue" type="text" class="form-control" value="<?php echo $this->input->post('venue'); ?>">
	    		</div>
	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("system_users"); ?>" class="btn btn-warning">Back</a>
	    	</div>
	    	</form>
	    </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>