<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<div class="container">
<div class="row">
	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if( ($concert->tickets_count==0) && ($concert->distributors_count==0) && ($concert->ticket_types_count==0) && ($concert->guests_count==0) ) { ?>
	    	 <a href="<?php echo site_url("concerts/delete/" . $concert->id); ?>" class="btn btn-danger btn-xs pull-right confirm">Delete</a>
<?php } ?>
	    		<h3 class="panel-title">Edit User Account</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

	    		<div class="form-group">
	    			<label>Concert Name</label>
	    			<input name="name" type="text" class="form-control" value="<?php echo $concert->name; ?>">
	    		</div>
	    		<div class="form-group">
	    			<label>Concert Date</label>
	    			<input name="date" type="text" class="form-control datepicker" value="<?php echo date('m/d/Y', strtotime($concert->date)); ?>">
	    		</div>
	    		<div class="form-group">
	    			<label>Venue</label>
	    			<input name="venue" type="text" class="form-control" value="<?php echo $concert->venue; ?>">
	    		</div>

	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("concerts"); ?>" class="btn btn-warning">Back</a>
	    	</div>
	    	</form>
	    </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>