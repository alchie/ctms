<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guests extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'Tickets - Guests');
		$this->template_data->set('current_page', 'Guests');
		$this->template_data->set('current_uri', 'guests');

		$this->load->model('Concerts_model');
		$this->load->model('Guests_model');
	}

	public function index($concert_id, $start=0) {
		$concert = new $this->Concerts_model;
		$concert->setId($concert_id,true);
		$this->template_data->set('concert', $concert->get());

		$guests = new $this->Guests_model;
		$guests->setConcertId($concert_id,true);
		$guests->set_start($start);
		$guests->set_order('id', 'ASC');
		if( $this->input->get('q') ) {
			$guests->set_where('name LIKE "%' . $this->input->get('q') . '%"');
			$guests->set_where_or('phone_number LIKE "%' . $this->input->get('q') . '%"');
			$guests->set_where_or('email LIKE "%' . $this->input->get('q') . '%"');
		}
		$this->template_data->set('guests', $guests->populate());
		
		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url("guests/index/{$concert_id}"),
			'total_rows' => $guests->count_all_results(),
			'per_page' => $guests->get_limit(),
		)));

		$this->load->view('concerts/guests/guests', $this->template_data->get_data());
	}

	public function add($concert_id) {

		$concert = new $this->Concerts_model;
		$concert->setId($concert_id,true);
		$this->template_data->set('concert', $concert->get());

		if( $this->input->post() ) {

			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required');

			if( $this->form_validation->run() ) {

				$guest = new $this->Guests_model;
				$guest->setConcertId($concert_id);
				$guest->setName($this->input->post('name'));
				$guest->setPhoneNumber($this->input->post('phone_number'));
				$guest->setEmail($this->input->post('email'));
				
				if( $guest->insert() ) {
					redirect("guests/edit/{$guest->getId()}");
				}
			
			}
		}

		$this->load->view('concerts/guests/guests_add', $this->template_data->get_data());
	}

	public function edit($id) {

		$guest = new $this->Guests_model;
		$guest->setId($id, true);
		$guest_data = $guest->get();

		$concert = new $this->Concerts_model;
		$concert->setId($guest_data->concert_id,true);
		$this->template_data->set('concert', $concert->get());

		if( $this->input->post() ) {

			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required');

			if( $this->form_validation->run() ) {

				$guest->setConcertId($guest_data->concert_id,true);
				$guest->setName($this->input->post('name'));
				$guest->setPhoneNumber($this->input->post('phone_number'));
				$guest->setEmail($this->input->post('email'));
				$guest->update();
			
			}
		}

		$this->template_data->set('guest', $guest->get());

		$this->load->view('concerts/guests/guests_edit', $this->template_data->get_data());
	}

	public function delete($id) {
		
		$guest = new $this->Guests_model;
		$guest->setId($id, true);
		$guest_data = $guest->get();
		$guest->delete();
		
		redirect(site_url("guests/index/{$guest_data->concert_id}"));
	}

}
