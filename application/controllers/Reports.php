<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'Tickets - Reports');
		$this->template_data->set('current_page', 'Reports');
		$this->template_data->set('current_uri', 'reports');

		$this->load->model('Concerts_model');
		$this->load->model('Tickets_model');
		$this->load->model('Ticket_types_model');
		$this->load->model('Distributors_model');
		$this->load->model('Distributor_tickets_model');
	}

	public function tickets($concert_id) {
		$concert = new $this->Concerts_model;
		$concert->setId($concert_id,true);
		$this->template_data->set('concert', $concert->get());

		$tickets = new $this->Tickets_model;
		$tickets->setConcertId($concert_id,true);
		$tickets->set_join('ticket_types', 'ticket_types.id=tickets.type_id');
		$tickets->set_join('distributor_tickets', 'distributor_tickets.ticket_number=tickets.ticket_number');
		$tickets->set_join('distributors', 'distributors.id=distributor_tickets.distributor_id');
		$tickets->set_select('tickets.*');
		$tickets->set_select('ticket_types.name');
		$tickets->set_select('ticket_types.price');
		$tickets->set_select('distributors.name as assigned_to');
		$tickets->set_select('distributors.id as distributor_id');
		$tickets->set_limit(0);
		$tickets->set_order('ticket_number', 'ASC');
		if( $this->input->get('distributor') ) {
			$tickets->set_where('distributors.id', $this->input->get('distributor') );          
		}
		$this->template_data->set('tickets', $tickets->populate());
		$this->template_data->set('tickets_count', $tickets->count_all_results());
		
		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url("tickets/index/{$concert_id}"),
			'total_rows' => $tickets->count_all_results(),
			'per_page' => $tickets->get_limit(),
		)));

		$this->load->view('concerts/reports/reports_ticket', $this->template_data->get_data());
	}


}
