<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Distributors extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'Tickets - Distributors');
		$this->template_data->set('current_page', 'Distributors');
		$this->template_data->set('current_uri', 'distributors');

		$this->load->model('Concerts_model');
		$this->load->model('Tickets_model');
		$this->load->model('Distributors_model');
		$this->load->model('Distributor_tickets_model');
	}

	public function index($concert_id, $start=0) {
		$concert = new $this->Concerts_model;
		$concert->setId($concert_id,true);
		$this->template_data->set('concert', $concert->get());

		$distributors = new $this->Distributors_model;
		$distributors->setConcertId($concert_id,true);
		$distributors->set_select("*");
		$distributors->set_select("(SELECT COUNT(*) FROM `distributor_tickets` WHERE concert_id = distributors.concert_id AND distributor_id = distributors.id) as tickets_assigned");
		$distributors->set_order('name', 'ASC');
		$distributors->set_start($start);
		if( $this->input->get('q') ) {
			$distributors->set_where('name LIKE "%' . $this->input->get('q') . '%"');
		}
		$this->template_data->set('distributors', $distributors->populate());
		
		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url("distributors/index/{$concert_id}"),
			'total_rows' => $distributors->count_all_results(),
			'per_page' => $distributors->get_limit(),
		)));

		$this->load->view('concerts/distributors/distributors', $this->template_data->get_data());
	}

	public function add($concert_id) {

		$concert = new $this->Concerts_model;
		$concert->setId($concert_id,true);
		$this->template_data->set('concert', $concert->get());

		if( $this->input->post() ) {

			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|required');

			if( $this->form_validation->run() ) {

				$distributors = new $this->Distributors_model;
				$distributors->setConcertId($concert_id);
				$distributors->setName($this->input->post('name'));
				$distributors->setPhoneNumber($this->input->post('phone_number'));
				$distributors->insert();
			}
			redirect("distributors/index/{$concert_id}");
		}

		$this->load->view('concerts/distributors/distributors_add', $this->template_data->get_data());
	}

	public function edit($id) {

		$distributors = new $this->Distributors_model;
		$distributors->setId($id, true);
		$distributors_data = $distributors->get();

		$concert = new $this->Concerts_model;
		$concert->setId($distributors_data->concert_id,true);
		$this->template_data->set('concert', $concert->get());

		if( $this->input->post() ) {

			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|required');

			if( $this->form_validation->run() ) {

				$distributors->setConcertId($distributors_data->concert_id,true);
				$distributors->setName($this->input->post('name'));
				$distributors->setPhoneNumber($this->input->post('phone_number'));
				$distributors->update();
				
			}
		}

		$this->template_data->set('distributor', $distributors->get());

		$this->load->view('concerts/distributors/distributors_edit', $this->template_data->get_data());
	}


	public function tickets($distributor_id, $start=0) {

		$distributors = new $this->Distributors_model;
		$distributors->setId($distributor_id, true);
		$distributors_data = $distributors->get();
		$this->template_data->set('distributor', $distributors_data);

		$concert = new $this->Concerts_model;
		$concert->setId($distributors_data->concert_id,true);
		$this->template_data->set('concert', $concert->get());

		$tickets = new $this->Distributor_tickets_model;
		$tickets->setDistributorId($distributor_id,true);
		$tickets->set_join('tickets', 'distributor_tickets.ticket_number=tickets.ticket_number');
		$tickets->set_join('ticket_types', 'ticket_types.id=tickets.type_id');
		$tickets->set_order('tickets.ticket_number', 'ASC');
		$tickets->set_select('tickets.*');
		$tickets->set_select('ticket_types.*');
		$tickets->set_select('distributor_tickets.*');
		$tickets->set_start($start);
		if( $this->input->get('q') ) {
			$tickets->set_where('tickets.ticket_number', $this->input->get('q'));
		}
		$this->template_data->set('tickets', $tickets->populate()); 

		$this->template_data->set('tickets_count', $tickets->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url("distributors/tickets/{$distributor_id}"),
			'total_rows' => $tickets->count_all_results(),
			'per_page' => $tickets->get_limit(),
		)));

		$this->load->view('concerts/distributors/distributors_tickets', $this->template_data->get_data());
	}

	public function tickets_add($distributor_id) {

		$distributors = new $this->Distributors_model;
		$distributors->setId($distributor_id, true);
		$distributors_data = $distributors->get();
		$this->template_data->set('distributor', $distributors_data);

		$concert = new $this->Concerts_model;
		$concert->setId($distributors_data->concert_id,true);
		$this->template_data->set('concert', $concert->get());

		if( $this->input->post() ) {

			$this->form_validation->set_rules('ticket_number', 'Ticket Number', 'trim|required');
			$this->form_validation->set_rules('date_released', 'Date Released', 'trim|required');

			if( $this->form_validation->run() ) {

				if( strpos($this->input->post('ticket_number'), "-") ) {

				$ticket_numbers = explode("-", $this->input->post('ticket_number'));

				$start = intval($ticket_numbers[0]);
				$end = intval($ticket_numbers[1]);

					if( is_int( $start ) && is_int( $end ) ) {

						for($i=$start;$i<=$end;$i++) {

							$ticket = new $this->Distributor_tickets_model;
							$ticket->setConcertId($distributors_data->concert_id, true);
							$ticket->setDistributorId( $distributor_id );
							$ticket->setTicketNumber( $i, true );
							$ticket->setDateReleased( date('Y-m-d', strtotime($this->input->post('date_released'))) );

							$tickets2 = new $this->Tickets_model;
							$tickets2->setConcertId($distributors_data->concert_id,true);
							$tickets2->setTicketNumber($i, true);

							if( $tickets2->nonEmpty() ) {
								if( $ticket->nonEmpty() === false ) {
									$ticket->insert();
								} else {
									$ticket->setDistributorId( $distributor_id, true );
									$ticket->update();
								}
							}

						}

					}

				} else {

					$ticket = new $this->Distributor_tickets_model;
					$ticket->setConcertId($distributors_data->concert_id, true);
					$ticket->setDistributorId( $distributor_id );
					$ticket->setTicketNumber( $this->input->post('ticket_number'), true );
					$ticket->setDateReleased( date('Y-m-d', strtotime($this->input->post('date_released'))) );

					$tickets2 = new $this->Tickets_model;
					$tickets2->setConcertId($distributors_data->concert_id,true);
					$tickets2->setTicketNumber($this->input->post('ticket_number'), true);

					if( $tickets2->nonEmpty() ) {
						if( $ticket->nonEmpty() === false ) {
							$ticket->insert();
						} else {
							$ticket->setDistributorId( $distributor_id, true );
							$ticket->update();
						}
					}

				}

			}

			redirect( site_url("distributors/tickets_add/{$distributor_id}")  );

		}

		$this->load->view('concerts/distributors/distributors_tickets_add', $this->template_data->get_data());
	}

	public function ticket_delete($concert_id, $distributor_id, $ticket_number) {
		
		$ticket = new $this->Distributor_tickets_model;
		$ticket->setConcertId( $concert_id, true );
		$ticket->setDistributorId( $distributor_id, true );
		$ticket->setTicketNumber( $ticket_number, true );
		$ticket->delete();
		
		redirect(site_url("distributors/tickets/{$distributor_id}"));
	}

	public function delete($id) {
		
		$distributors = new $this->Distributors_model;
		$distributors->setId($id, true);
		$distributors_data = $distributors->get();
		$distributors->delete();
		
		redirect(site_url("distributors/index/{$distributors_data->concert_id}"));
	}

}
