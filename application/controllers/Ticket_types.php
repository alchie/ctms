<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket_types extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'Tickets - Ticket Types');
		$this->template_data->set('current_page', 'Tickets Types');
		$this->template_data->set('current_uri', 'ticket_types');

		$this->load->model('Concerts_model');
		$this->load->model('Ticket_types_model');
	}

	public function index($concert_id) {
		$concert = new $this->Concerts_model;
		$concert->setId($concert_id,true);
		$this->template_data->set('concert', $concert->get());

		$types = new $this->Ticket_types_model;
		$types->setConcertId($concert_id,true);
		if( $this->input->get('q') ) {
			$types->set_where('name LIKE "%' . $this->input->get('q') . '%"');
		}
		$types->set_select('*');
		$types->set_select("(SELECT COUNT(*) FROM tickets WHERE type_id=ticket_types.id AND concert_id={$concert_id}) as tickets_count");
		$this->template_data->set('types', $types->populate());

		$this->load->view('concerts/ticket_types/ticket_types', $this->template_data->get_data());
	}

	public function add($concert_id) {

		$concert = new $this->Concerts_model;
		$concert->setId($concert_id,true);
		$this->template_data->set('concert', $concert->get());

		if( $this->input->post() ) {

			$this->form_validation->set_rules('name', 'Type Name', 'trim|required');
			$this->form_validation->set_rules('price', 'Price', 'trim|required');

			if( $this->form_validation->run() ) {
				$type = new $this->Ticket_types_model;
				$type->setName($this->input->post('name'));
				$type->setPrice($this->input->post('price'));
				$type->setConcertId($concert_id);
				if( $type->insert() ) {
					redirect("ticket_types/edit/{$type->getId()}");
				}
			}
		}
		$this->load->view('concerts/ticket_types/ticket_types_add', $this->template_data->get_data());
	}

	public function edit($id) {
		$type = new $this->Ticket_types_model;
		$type->setId($id, true);
		$type_data = $type->get();

		$concert = new $this->Concerts_model;
		$concert->setId($type_data->concert_id,true);
		$this->template_data->set('concert', $concert->get());

		if( $this->input->post() ) {

			$this->form_validation->set_rules('name', 'Type Name', 'trim|required');
			$this->form_validation->set_rules('price', 'Price', 'trim|required');
			if( $this->form_validation->run() ) {
				$type->setName($this->input->post('name'));
				$type->setPrice($this->input->post('price'));
				if( $type->nonEmpty() ) {
					$results = $type->getResults();
					$type->setConcertId( $results->concert_id );
					$type->set_exclude('id', 'concert_id');
					$type->update();
				} 
			}
		}

		$this->template_data->set('type', $type->get());

		$this->load->view('concerts/ticket_types/ticket_types_edit', $this->template_data->get_data());
	}

	public function delete($id) {
		if($this->session->user_id != $id) {
			$ticket = new $this->Tickets_model;
			$ticket->setId($id, true);
			$ticket->delete();
			redirect(site_url("types") . "?success=true" );
		}
		redirect(site_url("types") . "?error=true" );
	}

}
