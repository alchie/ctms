<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tickets extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'Tickets - Tickets');
		$this->template_data->set('current_page', 'Tickets');
		$this->template_data->set('current_uri', 'tickets');

		$this->load->model('Concerts_model');
		$this->load->model('Tickets_model');
		$this->load->model('Ticket_types_model');
		$this->load->model('Distributors_model');
		$this->load->model('Distributor_tickets_model');
	}

	private function _search_redirect($concert_id) {
		if( $this->input->get('q') != '' ) {
			redirect( site_url('tickets/index/' . $concert_id ) . "?q=" . $this->input->get('q') );
		}
	}

	private function _index($concert_id, $start=0, $output='index') {
		$concert = new $this->Concerts_model;
		$concert->setId($concert_id,true);
		$concert->set_select("*");
		$concert->set_select('(SELECT COUNT(*) FROM tickets) as tickets_count');
		$concert->set_select('(SELECT COUNT(*) FROM tickets WHERE concert_id=concerts.id AND paid=1) as sold');
		$concert->set_select('(SELECT COUNT(*) FROM tickets WHERE concert_id=concerts.id AND paid=0) as unsold');
		$concert->set_select('(SELECT COUNT(*) FROM distributor_tickets WHERE concert_id=concerts.id) as assigned');
		$concert->set_select('( SELECT(tickets_count) - (SELECT COUNT(*) FROM distributor_tickets WHERE concert_id=concerts.id)) as unassigned');
		$this->template_data->set('concert', $concert->get());

		$tickets = new $this->Tickets_model;
		$tickets->setConcertId($concert_id,true);
		$tickets->set_join('ticket_types', 'ticket_types.id=tickets.type_id');
		$tickets->set_join('distributor_tickets', 'distributor_tickets.ticket_number=tickets.ticket_number');
		$tickets->set_join('distributors', 'distributors.id=distributor_tickets.distributor_id');
		$tickets->set_select('tickets.*');
		$tickets->set_select('ticket_types.name');
		$tickets->set_select('ticket_types.price');
		$tickets->set_select('distributors.name as assigned_to');
		$tickets->set_select('distributors.id as distributor_id');
		$tickets->set_start($start);
		$tickets->set_order('ticket_number', 'ASC');

		if( $this->input->get('q') ) {
			$tickets->set_where('tickets.ticket_number', $this->input->get('q') );          
		}

		switch ($output) {
			case 'sold':
				$tickets->set_where('tickets.paid=1');   
				break;

			case 'unsold':
				$tickets->set_where('tickets.paid=0');   
				break;

			case 'assigned':
				$tickets->set_where('distributor_tickets.distributor_id IS NOT NULL');   
				break;
			
			case 'unassigned':
				$tickets->set_where('distributor_tickets.distributor_id IS NULL');   
				break;

			default:
				# code...
				break;
		}

		$this->template_data->set('tickets', $tickets->populate());
		$this->template_data->set('output', $output);
		
		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url("tickets/{$output}/{$concert_id}"),
			'total_rows' => $tickets->count_all_results(),
			'per_page' => $tickets->get_limit(),
		)));

		$this->load->view('concerts/tickets/tickets', $this->template_data->get_data());
	}

	public function index($concert_id, $start=0) {
		$this->_index($concert_id, $start, 'index');
	}

	public function sold($concert_id, $start=0) {
		$this->_search_redirect($concert_id);
		$this->_index($concert_id, $start, 'sold');
	}

	public function unsold($concert_id, $start=0) {
		$this->_search_redirect($concert_id);
		$this->_index($concert_id, $start, 'unsold');
	}

	public function assigned($concert_id, $start=0) {
		$this->_search_redirect($concert_id);
		$this->_index($concert_id, $start, 'assigned');
	}

	public function unassigned($concert_id, $start=0) {
		$this->_search_redirect($concert_id);
		$this->_index($concert_id, $start, 'unassigned');
	}

	public function type($concert_id, $type_id, $start=0) {

		$this->_search_redirect($concert_id);

		$concert = new $this->Concerts_model;
		$concert->setId($concert_id,true);
		$concert->set_select("*");
		$concert->set_select('(SELECT COUNT(*) FROM tickets) as tickets_count');
		$concert->set_select('(SELECT COUNT(*) FROM tickets WHERE concert_id=concerts.id AND paid=1) as sold');
		$concert->set_select('(SELECT COUNT(*) FROM tickets WHERE concert_id=concerts.id AND paid=0) as unsold');
		$concert->set_select('(SELECT COUNT(*) FROM distributor_tickets WHERE concert_id=concerts.id) as assigned');
		$concert->set_select('( SELECT(tickets_count) - (SELECT COUNT(*) FROM distributor_tickets WHERE concert_id=concerts.id)) as unassigned');
		$this->template_data->set('concert', $concert->get());

		$tickets = new $this->Tickets_model;
		$tickets->setConcertId($concert_id,true);
		$tickets->set_join('ticket_types', 'ticket_types.id=tickets.type_id');
		$tickets->set_join('distributor_tickets', 'distributor_tickets.ticket_number=tickets.ticket_number');
		$tickets->set_join('distributors', 'distributors.id=distributor_tickets.distributor_id');
		$tickets->set_select('tickets.*');
		$tickets->set_select('ticket_types.name');
		$tickets->set_select('ticket_types.price');
		$tickets->set_select('distributors.name as assigned_to');
		$tickets->set_select('distributors.id as distributor_id');
		$tickets->set_start($start);
		$tickets->set_order('ticket_number', 'ASC');
		$tickets->set_where('type_id', $type_id );          
		
		$this->template_data->set('tickets', $tickets->populate());

		$this->template_data->set('tickets_count', $tickets->count_all_results());
		
		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 5,
			'base_url' => base_url("tickets/type/{$concert_id}/{$type_id}"),
			'total_rows' => $tickets->count_all_results(),
			'per_page' => $tickets->get_limit(),
		)));

		$this->load->view('concerts/tickets/tickets', $this->template_data->get_data());
	}

	public function add($concert_id) {

		$this->_search_redirect($concert_id);

		$concert = new $this->Concerts_model;
		$concert->setId($concert_id,true);
		$this->template_data->set('concert', $concert->get());

		$types = new $this->Ticket_types_model;
		$types->setConcertId($concert_id,true);
		$this->template_data->set('types', $types->populate());

		if( $this->input->post() ) {

			$this->form_validation->set_rules('type_id', 'Ticket Type', 'trim|required');
			$this->form_validation->set_rules('ticket_number', 'Ticket Number', 'trim|required');

			if( $this->form_validation->run() ) {

			if( strpos($this->input->post('ticket_number'), "-") ) {

			$ticket_numbers = explode("-", $this->input->post('ticket_number'));

			$start = intval($ticket_numbers[0]);
			$end = intval($ticket_numbers[1]);

				if( is_int( $start ) && is_int( $end ) ) {
					for($i=$start;$i<=$end;$i++) {
						$ticket = new $this->Tickets_model;
						$ticket->setTypeId($this->input->post('type_id'));
						$ticket->setConcertId($concert_id,true);
						$ticket->setTicketNumber($i,true);
						$ticket->setPaid(0);
						if( $ticket->nonEmpty() === false ) {
							$ticket->insert();
						} else {
							$ticket->update();
						}
					}
				}

			} else {

				$ticket = new $this->Tickets_model;
				$ticket->setTypeId($this->input->post('type_id'));
				$ticket->setConcertId($concert_id,true);
				$ticket->setTicketNumber($this->input->post('ticket_number'),true);
				$ticket->setPaid(0);
				if( $ticket->nonEmpty() === false ) {
					$ticket->insert();
				} else {
					$ticket->update();
				}

			}
				redirect("tickets/index/{$concert_id}");
			
			}
		}

		$this->load->view('concerts/tickets/tickets_add', $this->template_data->get_data());
	}

	public function mark_paid($id) {

		$ticket = new $this->Tickets_model;
		$ticket->setId($id, true);
		$ticket->setPaid(1, false, true);
		$ticket->update();

		redirect( $this->agent->referrer() );

	}

	public function remove_assignment($id) {
		$ticket = new $this->Tickets_model;
		$ticket->setId($id, true);
		$ticket_data = $ticket->get();

		$assignment = new $this->Distributor_tickets_model;
		$assignment->setTicketNumber($ticket_data->ticket_number,true);
		$assignment->setConcertId($ticket_data->concert_id,true);
		$assignment->delete();

		redirect( $this->agent->referrer() );
	}

	public function edit($id) {

		$ticket = new $this->Tickets_model;
		$ticket->setId($id, true);
		$ticket->set_join('distributor_tickets', 'distributor_tickets.ticket_number=tickets.ticket_number');
		$ticket->set_join('distributors', 'distributors.id=distributor_tickets.distributor_id');
		$ticket->set_select('tickets.*');
		$ticket->set_select('distributors.id as distributor_id');
		$ticket->set_select('distributor_tickets.date_released as date_released');
		$ticket_data = $ticket->get();

		$this->_search_redirect($ticket_data->concert_id);

		$concert = new $this->Concerts_model;
		$concert->setId($ticket_data->concert_id,true);
		$this->template_data->set('concert', $concert->get());

		$types = new $this->Ticket_types_model;
		$types->setConcertId($ticket_data->concert_id,true);
		$this->template_data->set('types', $types->populate());

		if( $this->input->post() ) {

			$this->form_validation->set_rules('type_id', 'Ticket Type', 'trim|required');
			$this->form_validation->set_rules('ticket_number', 'Ticket Number', 'trim|required');
			if( $this->form_validation->run() ) {
				$ticket = new $this->Tickets_model;
				$ticket->setId($id, true);
				$ticket->setTypeId($this->input->post('type_id'));
				$ticket->setConcertId($ticket_data->concert_id);
				$ticket->setTicketNumber($this->input->post('ticket_number'));
				$ticket->setPaid($this->input->post('paid') ? 1 : 0);
				if( $ticket->nonEmpty() ) {
					$ticket->set_exclude('id','concert_id');
					$ticket->update();
				} 
			}

			if( $this->input->post('assign_to') != $ticket_data->distributor_id ) {
				if( $this->input->post('assign_to') == 'no_assignment') {
					$assignment = new $this->Distributor_tickets_model;
					$assignment->setTicketNumber($this->input->post('ticket_number'),true);
					$assignment->setConcertId($ticket_data->concert_id,true);
					$assignment->delete();
				} else {
					$assignment = new $this->Distributor_tickets_model;
					$assignment->setTicketNumber($this->input->post('ticket_number'),true);
					$assignment->setConcertId($ticket_data->concert_id,true);
					$assignment->setDistributorId($this->input->post('assign_to'));
					$assignment->setDateReleased( date('Y-m-d', strtotime($this->input->post('date_released')) ) );
					if( $assignment->nonEmpty() ) {
						$assignment->update();
					} else {
						$assignment->insert();
					}
				}
			}

			redirect("tickets/edit/{$id}");
		}

		
		$this->template_data->set('ticket', $ticket->get()); 

		$distributors = new $this->Distributors_model;
		$distributors->setConcertId($ticket_data->concert_id,true);
		$distributors->set_limit(0);
		$distributors->set_order('name', 'ASC');

		$this->template_data->set('distributors', $distributors->populate()); 

		$this->load->view('concerts/tickets/tickets_edit', $this->template_data->get_data());
	}

	public function delete($id) {
		
		$ticket = new $this->Tickets_model;
		$ticket->setId($id, true);
		$ticket_data = $ticket->get();
		$ticket->delete();
		
		redirect(site_url("tickets/index/{$ticket_data->concert_id}") . "?error=true" );
	}

}
