<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Concerts extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'Tickets - Concerts');
		$this->template_data->set('current_page', 'Concerts');
		$this->template_data->set('current_uri', 'concerts');

		$this->load->model('Concerts_model');
	}

	public function index() {
		$concerts = new $this->Concerts_model;
		$concerts->set_select('*');
		$concerts->set_select('(SELECT COUNT(*) FROM tickets WHERE concert_id=concerts.id) as tickets_count');
		//$concert->set_select('(SELECT COUNT(*) FROM distributors WHERE concert_id=concerts.id) as distributors_count');
		$concerts->set_select('(SELECT COUNT(*) FROM distributor_tickets JOIN distributors ON distributors.id=distributor_tickets.distributor_id WHERE distributors.concert_id=concerts.id) as tickets_assigned_count');
		//$concert->set_select('(SELECT COUNT(*) FROM ticket_types WHERE concert_id=concerts.id) as ticket_types_count');
		//$concert->set_select('(SELECT COUNT(*) FROM guests WHERE concert_id=concerts.id) as guests_count');
		$this->template_data->set('concerts', $concerts->populate());
		$this->load->view('concerts/concerts/concerts', $this->template_data->get_data());
	}

	public function add() {
		if( $this->input->post() ) {
			$this->form_validation->set_rules('name', 'Concert Name', 'trim|required');
			$this->form_validation->set_rules('date', 'Concert Date', 'trim|required');
			$this->form_validation->set_rules('venue', 'Venue', 'trim|required');
			if( $this->form_validation->run() ) {
				$concert = new $this->Concerts_model;
				$concert->setName($this->input->post('name'));
				$concert->setDate( date('Y-m-d', strtotime($this->input->post('date'))));
				$concert->setVenue($this->input->post('venue'));
				if( $concert->insert() ) {
					redirect("concerts/edit/{$concert->getId()}");
				}
			}
		}
		$this->load->view('concerts/concerts/concerts_add', $this->template_data->get_data());
	}

	public function edit($id) {
		$concert = new $this->Concerts_model;
		$concert->setId($id, true);

		if( $this->input->post() ) {
			$this->form_validation->set_rules('name', 'Concert Name', 'trim|required');
			$this->form_validation->set_rules('date', 'Concert Date', 'trim|required');
			$this->form_validation->set_rules('venue', 'Venue', 'trim|required');
			if( $this->form_validation->run() ) {
				$concert->setName($this->input->post('name'));
				$concert->setDate( date('Y-m-d', strtotime($this->input->post('date'))));
				$concert->setVenue($this->input->post('venue'));
				if( $concert->nonEmpty() ) {
					$concert->set_exclude('id');
					$concert->update();
				} 
			}
		}

		$concert->set_select('*');
		$concert->set_select('(SELECT COUNT(*) FROM tickets WHERE concert_id=concerts.id) as tickets_count');
		$concert->set_select('(SELECT COUNT(*) FROM distributors WHERE concert_id=concerts.id) as distributors_count');
		$concert->set_select('(SELECT COUNT(*) FROM distributor_tickets JOIN distributors ON distributors.id=distributor_tickets.distributor_id WHERE distributors.concert_id=concerts.id) as tickets_assigned_count');
		$concert->set_select('(SELECT COUNT(*) FROM ticket_types WHERE concert_id=concerts.id) as ticket_types_count');
		$concert->set_select('(SELECT COUNT(*) FROM guests WHERE concert_id=concerts.id) as guests_count');
		$this->template_data->set('concert', $concert->get());

		$this->load->view('concerts/concerts/concerts_edit', $this->template_data->get_data());
	}

	public function delete($id) {
		if($this->session->user_id != $id) {
			$concert = new $this->Concerts_model;
			$concert->setId($id, true);
			$concert->delete();
			redirect(site_url("concerts") . "?success=true" );
		}
		redirect(site_url("concerts") . "?error=true" );
	}

}
